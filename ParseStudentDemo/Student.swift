//
//  Student.swift
//  ParseStudentDemo
//
//  Created by James Cash on 20-09-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

import Foundation
import Parse

class Student: PFObject, PFSubclassing {

    @NSManaged var name: String

    convenience init(name: String) {
        self.init()
        self.name = name
    }

    static func parseClassName() -> String {
        return "Student"
    }
}
