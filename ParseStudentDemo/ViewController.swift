//
//  ViewController.swift
//  ParseStudentDemo
//
//  Created by James Cash on 19-09-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

import UIKit
import Parse

class ViewController: UITableViewController {

    var students: [Student] = []
//    var students: [PFObject] = []
    var textfield: UITextField? = nil

    func getStudents() {

//        let query = Student.query(with: NSPredicate(format: "name contains 'a'", argumentArray: nil))
        let query = Student.query()
        query?.whereKey("name", contains: "a")
        query?.addAscendingOrder("name")
//        let query = PFQuery(className: "Student")
        query?.findObjectsInBackground { (objects, err) in
            self.students = objects! as! [Student]
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        getStudents()
    }

    @IBAction func addStudent(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Add Students", message: nil, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Add", style: .default) { (action) in
            let name = self.textfield!.text!
            let student = Student(name: name)
//            student["name"] = name
            student.saveInBackground(block: { (success, err) in
                if success {
                    print("Yay, saved!")
                } else {
                    print("Oh no!")
                }
                self.getStudents()
            })
//            let student = Student(name: name)
//            self.students.append(student)
//            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
        }

        alert.addTextField { (textfield) in
            self.textfield = textfield
        }

        alert.addAction(cancelAction)
        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

//        cell.textLabel?.text = students[indexPath.row]["name"] as! String
        cell.textLabel?.text = students[indexPath.row].name

        return cell
    }

}
